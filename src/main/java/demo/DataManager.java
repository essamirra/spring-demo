package demo;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

/**
 * Created by Proinina Maria
 */
public class DataManager implements IDataManager {

    private String DATABASE_URL;
    private String LOGIN;
    private String PASSWORD;
    private Connection connection = null;
    private PreparedStatement findEmploeeById = null;
    private PreparedStatement addEmploee = null;
    private PreparedStatement deleteEmploee = null;
    private PreparedStatement findEmploeeByJob = null;
    private PreparedStatement findEmploeeByDepartment = null;

    public DataManager(String url, String login, String password) {
        DATABASE_URL = url;
        LOGIN = login;
        PASSWORD = password;
    }


    @Override
    public void connectToDatabase() throws ClassNotFoundException, SQLException {
        if (connection != null) return;
        Locale.setDefault(Locale.ENGLISH);
        Class.forName("oracle.jdbc.driver.OracleDriver");
        connection = DriverManager.getConnection(DATABASE_URL, LOGIN, PASSWORD);

    }

    @Override
    public Employee findEmploee(int id) throws SQLException {
        if (connection == null) return null;
        PreparedStatement findEmploeeById = connection.prepareStatement("Select * from emp where empno = ?");
        findEmploeeById.setInt(1, id);
        ResultSet resultSet = findEmploeeById.executeQuery();
        resultSet.next();

        Employee result = buildEmploeeFromResultSet(resultSet);
        resultSet.close();
        findEmploeeById.close();
        return result;
    }

    @Override
    public List<Employee> findEmploeeByName(String job) throws SQLException {
        if (connection == null) return null;
        findEmploeeByJob = connection.prepareStatement("Select * from emp where job = ?");
        findEmploeeByJob.setString(1, job);
        ResultSet resultSet = findEmploeeByJob.executeQuery();
        List<Employee> result = new ArrayList<>();
        while (resultSet.next()) {
            result.add(buildEmploeeFromResultSet(resultSet));
        }
        resultSet.close();
        findEmploeeByJob.close();
        return result;
    }

    @Override
    public List<Employee> findEmploeeByDept(int deptno) throws SQLException {
        if (connection == null) return null;
        findEmploeeByDepartment = connection.prepareStatement("Select * from emp where deptno  = ?");
        findEmploeeByDepartment.setInt(1, deptno);
        ResultSet resultSet = findEmploeeByDepartment.executeQuery();
        List<Employee> result = new ArrayList<>();
        while (resultSet.next()) {
            result.add(buildEmploeeFromResultSet(resultSet));
        }
        resultSet.close();
        findEmploeeByDepartment.close();
        return result;
    }

    @Override
    public void insertEmployee(Employee e) throws SQLException {
        if (connection == null) return;
        addEmploee = connection.prepareStatement("Insert into emp values (?, ?, ?, ?, ?, ?, ?, ?)");
        addEmploee.setInt(1, e.getEmpno());
        addEmploee.setString(2, e.getEname());
        addEmploee.setString(3, e.getJob());
        addEmploee.setInt(4, e.getMgr());
        addEmploee.setDate(5, e.getHiredate());
        addEmploee.setDouble(6, e.getSal());
        addEmploee.setDouble(7, e.getComm());
        addEmploee.setInt(8, e.getDeptno());
        addEmploee.executeUpdate();
        addEmploee.close();
    }

    private Employee buildEmploeeFromResultSet(ResultSet resultSet) throws SQLException {

        int id = resultSet.getInt("empno");
        int deptno = resultSet.getInt("deptno");
        String ename = resultSet.getString("ename");
        String job = resultSet.getString("job");
        double sal = resultSet.getDouble("sal");
        int mgr = resultSet.getInt("mgr");
        double comm = resultSet.getDouble("comm");
        Date hiredate = resultSet.getDate("hiredate");
        return new Employee(id, deptno, sal, ename, job, mgr, hiredate, comm);
    }

    @Override
    public void deleteEmploee(int id) throws SQLException {
        deleteEmploee = connection.prepareStatement("Delete from emp where empno = ?");
        deleteEmploee.setInt(1, id);
        deleteEmploee.executeUpdate();
        deleteEmploee.close();
    }


    @Override
    public void closeConnection() throws SQLException {
        if (connection != null) {
            connection.close();
        }

    }

}
