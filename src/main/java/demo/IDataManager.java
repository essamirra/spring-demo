package demo;

import java.sql.SQLException;
import java.util.List;

/**
 * Created by maria on 18.04.2017.
 */
public interface IDataManager {
    void connectToDatabase() throws ClassNotFoundException, SQLException;

    Employee findEmploee(int id) throws SQLException;

    List<Employee> findEmploeeByName(String job) throws SQLException;

    List<Employee> findEmploeeByDept(int deptno) throws SQLException;

    void insertEmployee(Employee e) throws SQLException;

    void deleteEmploee(int id) throws SQLException;

    void closeConnection() throws SQLException;
}
