package demo;

/**
 * Created by maria on 17.04.2017.
 */
public interface IPresenter {
    String EXIT_STRING = "exit";

    void loadMenu();

    void loadOperationMenu(int index);

    void proceedStringInput(String input);

    void proceedIntInput(int input);

    void proceedEmployeeInput(Employee e);

    void exit();

    void start();
}
