package demo;

import demo.config.DataLayer;
import demo.config.PresentationLayer;
import demo.config.ViewLayer;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.context.ConfigurableApplicationContext;

@SpringBootApplication
public class DemoApplication {

    public static void main(String[] args) {
        SpringApplicationBuilder appBuilder =
                new
                        SpringApplicationBuilder(DataLayer.class, ViewLayer.class, PresentationLayer.class);

        ConfigurableApplicationContext root = appBuilder.run();
        IMenu m = root.getBean(IMenu.class);
        m.start();


    }
}
