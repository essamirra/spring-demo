package demo.config;

import demo.IMenu;
import demo.Menu;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * Created by maria on 17.04.2017.
 */
@Configuration
public class ViewLayer {
    @Bean
    public IMenu menu() {
        return new Menu();
    }
}
