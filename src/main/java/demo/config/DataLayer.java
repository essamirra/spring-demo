package demo.config;

import demo.DataManager;
import demo.IDataManager;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.context.support.PropertySourcesPlaceholderConfigurer;

/**
 * Created by maria on 18.04.2017.
 */
@Configuration
@PropertySource("classpath:database.properties")
public class DataLayer {
    @Value("${url}")
    String url;
    @Value("${login}")
    String login;
    @Value("${password}")
    String password;

    @Bean
    public static PropertySourcesPlaceholderConfigurer
    propertySourcesPlaceholderConfigurer() {
        return new PropertySourcesPlaceholderConfigurer();
    }

    @Bean
    IDataManager dataManager() {
        return new DataManager(url, login, password);
    }

}

