package demo.config;

import demo.DemoApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * Created by maria on 18.04.2017.
 */
@Configuration
public class RootLayer {
    @Bean
    DemoApplication mainRunner() {
        return new DemoApplication();
    }
}
