package demo.config;

import demo.IPresenter;
import demo.Presenter;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * Created by maria on 17.04.2017.
 */
@Configuration
public class PresentationLayer {
    @Bean
    IPresenter presenter() {
        return new Presenter();
    }
}
