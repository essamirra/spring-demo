package demo;

import org.springframework.beans.factory.annotation.Autowired;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.sql.Date;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Locale;
import java.util.Map;

/**
 * Created by Proinina Maria
 */
public class Menu implements IMenu {
    public static final String MESSAGE = "This is not an valid value. Input code again or input \"" + "exit" +
            "\" to exit";
    public static final String SOMETHING_GONE_WRONG_PLEASE_TRY_AGAIN = "Something gone wrong, please try again";

    private final BufferedReader reader = new BufferedReader(new InputStreamReader(
            System.in));
    @Autowired
    private IPresenter IPresenter;

    public Menu() {
    }

    @Override
    public void showMessage(String message) {
        System.out.println(message);
    }

    @Override
    public void showOperationInput() {
        int result;
        String message = "This is not an option code. Input code again or input \"" + IPresenter.EXIT_STRING +
                "\" to exit";
        result = getIntResultFromConsoleWithExitCheck(message);
        IPresenter.loadOperationMenu(result);
    }

    private int getIntResultFromConsoleWithExitCheck(String message) {
        int result = 0;
        String input = null;
        input = getStringFromConsole(input);
        needToExit(input);
        boolean flag = false;
        while (!flag) {
            try {
                result = Integer.parseInt(input);
                flag = true;
            } catch (NumberFormatException e) {

                showMessage(message);
            }
        }
        return result;
    }

    private double getDoubleResultFromConsoleWithExitCheck(String message) {
        double result = 0;
        String input = null;
        input = getStringFromConsole(input);
        needToExit(input);
        boolean flag = false;
        while (!flag) {
            try {
                result = Double.parseDouble(input);
                flag = true;
            } catch (NumberFormatException e) {

                showMessage(message);
            }
        }
        return result;
    }

    private String getStringFromConsole(String input) {
        try {
            input = reader.readLine();
            needToExit(input);
        } catch (IOException e) {
            showMessage(SOMETHING_GONE_WRONG_PLEASE_TRY_AGAIN);
            IPresenter.loadMenu();
        }
        return input;
    }

    private void needToExit(String input) {
        if (input.equals(IPresenter.EXIT_STRING))
            IPresenter.exit();
    }

    @Override
    public void showMenu(Map<Integer, String> menu) {
        for (Map.Entry<Integer, String> e : menu.entrySet()) {
            showMessage(e.toString());
        }
    }

    public boolean showDialogMessage() {
        showMessage("Continue? Type yes for continue and no for exit to menu");
        String input = null;
        try {
            input = reader.readLine();
            return input.equals("yes");
        } catch (IOException e) {
            showMessage(SOMETHING_GONE_WRONG_PLEASE_TRY_AGAIN);
            return false;
        }
    }

    @Override
    public void showStringParameterInput() {
        String input = null;
        input = getStringFromConsole(input);
        if (showDialogMessage())
            IPresenter.proceedStringInput(input);
        else
            IPresenter.loadMenu();

    }

    @Override
    public void showIntParameterInput() {
        int result = -1;
        result = getIntResultFromConsoleWithExitCheck(MESSAGE);
        if (showDialogMessage())
            IPresenter.proceedIntInput(result);
        else IPresenter.loadMenu();
    }

    @Override
    public void showEmployeeInput() {
        String name = null;
        String job = null;
        int id = 0;
        int deptId = 0;
        double sal = 0;
        double comm = 0;
        Date hiredate = null;
        int mgr = 0;
        try {
            showMessage("Input name");
            getStringFromConsole(name);
            showMessage("Input job");
            getStringFromConsole(job);
            showMessage("Input employee id");
            id = getIntResultFromConsoleWithExitCheck("This is not a valid id. Input id again or input \"" + IPresenter.EXIT_STRING +
                    "\" to exit");

            showMessage("Input department id");
            deptId = getIntResultFromConsoleWithExitCheck("This is not a valid department id. Input id again or input \"" + IPresenter.EXIT_STRING +
                    "\" to exit");
            showMessage("Input manager id");
            mgr = getIntResultFromConsoleWithExitCheck("This is not a valid manager id. Input id again or input \"" + IPresenter.EXIT_STRING +
                    "\" to exit");
            showMessage("Input salary");
            sal = getDoubleResultFromConsoleWithExitCheck("This is not a valid salary. Input id again or input \"" + IPresenter.EXIT_STRING +
                    "\" to exit");
            showMessage("Input commission");
            comm = getDoubleResultFromConsoleWithExitCheck("This is not a valid commission. Input id again or input \"" + IPresenter.EXIT_STRING +
                    "\" to exit");
            showMessage("Input hiredate in format dd-MM-YY");
            boolean flag = false;
            while (!flag) {
                try {
                    String tmp = reader.readLine();
                    needToExit(tmp);

                    DateFormat format = new SimpleDateFormat("dd-MM-yyyy", Locale.ENGLISH);
                    java.util.Date temp = format.parse(tmp);
                    hiredate = new Date(temp.getTime());
                    flag = true;
                } catch (ParseException e) {
                    showMessage("This is not a valid date. Input id again or input \"" + IPresenter.EXIT_STRING +
                            "\" to exit");
                }
            }

        } catch (IOException e) {
            showMessage(SOMETHING_GONE_WRONG_PLEASE_TRY_AGAIN);
            IPresenter.loadMenu();
        }
        if (showDialogMessage())
            IPresenter.proceedEmployeeInput(new Employee(id, deptId, sal, name, job, mgr, hiredate, comm));
        else
            IPresenter.loadMenu();
    }

    public void start() {
        IPresenter.start();
        IPresenter.loadMenu();
    }

    @Override
    public void exit() {
        try {
            reader.close();
        } catch (IOException e) {
            showMessage("Application closed with errors");
        }
    }
}
